angular.module('angularCdp.cdp-gauge', [])
    .component('cdpGauge', {
        transclude: true,
        bindings: {
            width: '@',
            routing: '@',
            options: '@',
            minValue: '@',
            maxValue: '@',
            displayTextField: '='
        },
        controllerAs: 'vm',
        controller: ['$scope', 'studioapi', '$element', function ($scope, studioapi, $element) {

            var vm = this;
            vm.subscriptions = [];

            function createGauge() {
                var opts = {
                    angle: -0.2, // The span of the gauge arc
                    lineWidth: 0.2, // The line thickness
                    radiusScale: 1, // Relative radius
                    pointer: {
                        length: 0.6, // // Relative to gauge radius
                        strokeWidth: 0.035, // The thickness
                        color: '#000000' // Fill color
                    },
                    limitMax: false,     // If false, the max value of the gauge will be updated if value surpass max
                    limitMin: false,     // If true, the min value of the gauge will be fixed unless you set it manually
                    colorStart: '#6F6EA0',   // Colors
                    colorStop: '#C0C0DB',    // just experiment with them
                    strokeColor: '#EEEEEE',  // to see which ones work best for you
                    generateGradient: true,
                    highDpiSupport: true     // High resolution support
                };
                var userOptions = $scope.$eval(vm.options);
                opts = angular.merge(opts, userOptions);

                var target = $element.find('canvas')[0];
                var gauge = new Gauge(target).setOptions(opts);
                var textField = $element.find('div').find('div')[0];
                gauge.setTextField(textField);
                if (vm.maxValue)
                    gauge.maxValue = vm.maxValue;
                if (vm.minValue)
                    gauge.setMinValue(vm.minValue);
                gauge.animationSpeed = 1;
                //gauge.set(vm.minValue || 0);
                gauge.set(0);

                studioapi.getClient().find(vm.routing).then(
                    function (node) {
                        vm.subscription = {node: node, callback: function (value) {
                            gauge.set(value);
                        }};
                        node.subscribeToValues(vm.subscription.callback);
                    }
                );
            }

            this.$postLink = createGauge;

            this.$onDestroy = function () {
                var s = vm.subscription;
                s.node.unsubscribeFromChildValues(s.callback);
                console.log("Unsubscribed " + JSON.stringify(s.node));
            };

        }],
        template: '\
          <div style="display: inline-block;" ng-style="{ width: vm.width }"> \
            <canvas id="{{::$id}}" style="width: 100%"></canvas> \
            <div ng-show="vm.displayTextField !== false" id="{{::$id + \'Text\'}}" \
                 style="font-size: 15pt; text-align: center; top: -25px; "></div> \
          </div>'
    });
