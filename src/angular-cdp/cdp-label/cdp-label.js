var app = angular.module("angularCdp.cdp-label", []);

app.directive('cdpLabel', ['studioapi', function(studioapi) {

    function link(scope, element, attrs) {

        var vm = scope;
        this.subscription = {};

        studioapi.getClient().find(attrs.routing).then(
            function (node) {
                var precision = attrs.precision;
                vm.subscription = {node: node, callback: function (value) {
                    if (precision && isNumeric(value))
                        value = value.toFixed(precision);
                    element.text(value);
                }};
                node.subscribeToValues(vm.subscription.callback);
            }
        );

        element.on('$destroy', function() {
            var s = vm.subscription;
            s.node.unsubscribeFromChildValues(s.callback);
        });

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    }

    return {
        restrict: 'E',
        scope: {
            routing: '=',
            precision: '='
        },
        template: '',
        link: link
    };
}]);
