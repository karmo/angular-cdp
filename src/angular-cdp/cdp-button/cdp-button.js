var app = angular.module("angularCdp.cdp-button", []);

app.directive('cdpButton', ['studioapi', function(studioapi) {

    function link(scope, element, attrs) {

        var vm = scope;

        studioapi.getClient().find(attrs.routing).then(
            function (node) {
                vm.node = node;
            }
        );

        var onButtonClick = function () {
            vm.node.setValue(true);
        };

        element.on('click', onButtonClick);

        scope.$on('$destroy', function () {
            element.off('click', onButtonClick);
        });
    }

    return {
        restrict: 'E',
        replace : true,
        scope: {
            routing: '@'
        },
        transclude: true,
        template: '<button ng-transclude></button>',
        link: link
    };
}]);
