var ANGULAR_CDP_STUDIOAPI_URL = window.location.host;

(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('angularCdp.config', [])
      .value('angularCdp.config', {
          debug: true
      });

  // Modules
  angular.module('angularCdp.directives', []);
  angular.module('angularCdp.services', []);
  angular.module('angularCdp',
      [
          'angularCdp.config',
          'angularCdp.cdp-bar',
          'angularCdp.cdp-button',
          'angularCdp.cdp-chart',
          'angularCdp.cdp-gauge',
          'angularCdp.cdp-input',
          'angularCdp.cdp-label'
      ]).
      service('studioapi', function() {
        var client = new studio.api.Client(ANGULAR_CDP_STUDIOAPI_URL);

        this.getClient = function() {
          return client;
        };
  });

})(angular);
