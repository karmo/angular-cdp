var app = angular.module("angularCdp.cdp-input", []);

app.directive('cdpInput', ['studioapi', function(studioapi) {

    function link(scope, element, attrs) {

        var vm = scope;
        vm.subscription = {};

        studioapi.getClient().find(attrs.routing).then(
            function (node) {
                vm.subscription = {node: node, callback: function (value) {
                    scope.$evalAsync(function () {
                        scope.model = value;
                    });
                }};
                node.subscribeToValues(vm.subscription.callback);
            }
        );

        scope.onValueChange = function(newValue) {
            if (vm.subscription.node)
                vm.subscription.node.setValue(newValue);
        };

        element.on('$destroy', function() {
            if (vm.subscription.node) {
                var s = vm.subscription;
                s.node.unsubscribeFromChildValues(s.callback);
            }
        });
    }

    return {
        restrict: 'E',
        replace: true,
        scope: {
            routing: '@',
        },
        template: '<input ng-model="model" ng-change="onValueChange(model)">',
        link: link
    };
}]);
