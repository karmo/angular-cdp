angular.module('angularCdp.cdp-chart', [])
    .component('cdpChart', {
        transclude: true,
        bindings: {
            width: '@',
            height: '@',
            options: '@',
            displayLegend: '='
        },
        controllerAs: 'vm',
        controller: ['$scope', 'studioapi', '$element', '$window',
            function ($scope, studioapi, $element, $window) {

            var vm = this;
            vm.subscriptions = [];
            vm.nodeNames = [];
            var chart;
            var options = {
                interpolation: 'step',
                grid: {fillStyle: '#f9f9f9'},
                labels: {fillStyle: '#2c1919', fontSize:12},
                timestampFormatter: SmoothieChart.timeFormatter,
                yRangeFunction: yRangeFunction
            };

            this.addSignal = function (nodeName, color) {
                color = color || nextColor();
                vm.nodeNames.push(nodeName);
                var series = new TimeSeries();
                if (!chart)
                    createTimeline();
                chart.addTimeSeries(series, {lineWidth: 2, strokeStyle: color});

                studioapi.getClient().find(nodeName).then(
                    function (node) {
                        var subscription = function (value, timestamp) {
                            var date = new Date(timestamp.toNumber() / 1000 / 1000);
                            series.append(date, value);
                        };
                        node.subscribeToValues(subscription);
                        vm.subscriptions.push({node: node, subscription: subscription});
                    }
                );
                return color;
            };

            var colors = ['blue', 'green', 'red', 'purple', 'black'];
            var colorIndex = 0;
            function nextColor() {
                return colors[colorIndex++ % colors.length];
            }

            function createTimeline() {
                var userOptions = $scope.$eval(vm.options);
                var mergedOptions = angular.merge(options, userOptions);
                chart = new SmoothieChart(mergedOptions);
                chart.streamTo($element.find('canvas')[0], 866);
            }

            function yRangeFunction(range) {
                if (this.min === null || isNaN(this.min))
                    this.min = range.min;
                if (this.max === null || isNaN(this.max))
                    this.max = range.max;

                this.min = Math.min(range.min, this.min);
                this.max = Math.max(range.max, this.max);

                var yRange = this.max - this.min;
                var extraSpace = yRange * 0.1; // Makes line more visible

                return {min: this.min - extraSpace, max: this.max + extraSpace};
            }

            this.$postLink = function () {
                setCanvasWidth();
            };

            this.$onDestroy = function () {
                for (var i = 0; i < vm.subscriptions.length; i++) {
                    var s = vm.subscriptions[i];
                    s.node.unsubscribeFromChildValues(s.subscription);
                }
            };

            function setCanvasWidth() {
                vm.canvasWidth = Math.min($element.parent()[0].offsetWidth, vm.width);
            }

            angular.element($window).bind('resize', function(){
                setCanvasWidth();
                $scope.$digest();
            });

        }],
        template: '\
          <div>\
            <canvas id="smoothieChart_{{::$id}}" width="{{vm.canvasWidth}}" height="{{vm.height}}"></canvas>\
            <div style="display: inline-block; vertical-align: top">\
              <span ng-transclude></span>\
            </div>\
          </div>'
    })
    .component('cdpChartLine', {
        require: {
            chartCtrl: '^cdpChart'
        },
        bindings: {
            routing: '@',
            color: '@'
        },
        controller: [function () {
            this.$onInit = function () {
                this.color = this.chartCtrl.addSignal(this.routing, this.color);
            };
        }],
        controllerAs: 'vm',
        template: '\
          <div ng-if="vm.chartCtrl.displayLegend !== false">\
            <div style="width: 8px;  height: 8px; margin-right: 5px; margin-left: 5px; background: {{vm.color}}; display: inline-block"></div> \
            <span>{{vm.routing}}</span>\
          </div>'
    });
