var app = angular.module("angularCdp.cdp-bar", []);

app.directive('cdpBar', ['studioapi', function (studioapi) {

    function link(scope, element, attrs) {

        var vm = scope;
        this.subscription = {};

        studioapi.getClient().find(attrs.routing).then(
            function (node) {
                vm.subscription = {
                    node: node, callback: function (value) {
                        element.progress('set progress', value);
                    }
                };
                node.subscribeToValues(vm.subscription.callback);
            }
        );

        element.on('$destroy', function () {
            var s = vm.subscription;
            s.node.unsubscribeFromChildValues(s.callback);
        });

        element.progress(scope.$eval(attrs.options));
    }

    return {
        restrict: 'E',
        replace : true,
        transclude: true,
        scope: {
            routing: '@',
            options: '@'
        },
        template: '\
            <div">\
                <div class="bar"><div class="progress"></div></div>\
                <div class="label" ng-transclude=""></div>\
            </div>',
        link: link
    };
}]);
