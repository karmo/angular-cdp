# Angular CDP

This library provides Angular bindings to communicate with [CDP systems](http://cdpstudio.com/) over StudioAPI.

Demo project can be found [here](https://gitlab.com/karmo/angular-cdp-demo). It is recommended to use it as template.

## Installation

Clone this project and install all dependencies with

    npm install

After that just include in your html file

    <script src="angular-cdp/dist/angular-cdp.js"></script>

### Dependencies

1. Everything under `bower` directory.
1. StudioAPI Javascript client that consists of files `studioapi.js` and `studioapi.proto`. Both are bundled with
   [CDP Studio](http://cdpstudio.com/).

The [Demo project](https://gitlab.com/karmo/angular-cdp-demo) already includes these dependencies.

## Usage

This library provides some directives, components and services to communicate with a CDP system.

### CDP Chart

Based on [Smoothie Charts](http://smoothiecharts.org/). Sample usage:

    <cdp-chart width="500" height="200" options='{
                    "labels": { "fontSize": 17 }
                }'>
        <cdp-chart-line routing="webApp.CPULoad"></cdp-chart-line>
        <cdp-chart-line routing="webApp.Sine.Output"></cdp-chart-line>
        <cdp-chart-line routing="webApp.MemUsedRelative" color="red"></cdp-chart-line>
    </cdp-chart>

Example above creates a chart with 3 CDP Signals. Font size is enlarged.

![Chart image](https://gitlab.com/karmo/angular-cdp/raw/master/images/chart.png)

`cdp-chart` element can have following  attributes.

Attribute | Description
---|---
width (required) | Maximum width of the chart canvas.
height (required) | Height of the chart canvas.
display-legend | Default is `true`. Displays legend with color and routing for each `cdp-chart-line`.
options | Smoothie Charts specific options as JSON. Can change colors, text size, axis length etc. Full list [here](http://smoothiecharts.org/).

Each `cdp-chart` element can have one or many `cdp-chart-line` elements which describe a plotted the CDP Signal.

Attribute | Description
---|---
color | Color of the line. When missing different colors are assigned to chart lines automatically.
routing (required) | Routing of a CDP object (signal, property etc.)


### CDP Gauge

Based on [Gauge.js](http://bernii.github.io/gauge.js/)

    <cdp-gauge max-value="1" routing="webApp.CPULoad" options='{ "angle": 0 }'></cdp-gauge>
    <cdp-gauge max-value="100" routing="webApp.CDPSignal"></cdp-gauge>

![Gauge image](https://gitlab.com/karmo/angular-cdp/raw/master/images/gauge.png)

Attribute | Description
---|---
routing (required) | Routing of a CDP object (signal, property etc.)
width | Width of the gauge canvas. Height is inferred from width.
options | Gauge.js specific options as JSON. Can change colors, angle etc. Full list [here](http://bernii.github.io/gauge.js/).
min-value | Minimum value on the gauge scale.
max-value | Maximum value on the gauge scale.
display-text-field | Default is `true`. Displays numeric value at the bottom of CDP Gauge.

### CDP Label

Simple directive that displayes a value from CDP System. Can be wrapped into other elements (like `h1`) for formatting.

    <h1>CPU Load: <cdp-label routing="webApp.CPULoad" precision="2"></cdp-label></h1>

![Label image](https://gitlab.com/karmo/angular-cdp/raw/master/images/label.png)

Attribute | Description
---|---
routing (required) | Routing of a CDP object (signal, property etc.)
precision | When routing a numeric value, this sets the number of decimal places to display.

### CDP Button

Used to send a CDP Message.

    <cdp-button routing="webApp.CM_SUSPEND" class="ui primary button">Button</cdp-button>

![Button image](https://gitlab.com/karmo/angular-cdp/raw/master/images/button.png)

Attribute | Description
---|---
routing (required) | Full path of the CDP Message to send.
[any other attribute] | Passed directly to inner `button` element. Can be used to customize the button. Example above uses `class` attribute to style the button using [Semantic UI](https://semantic-ui.com/).

### CDP Input

Element to display or update any value in CDP System.

Attribute | Description
---|---
routing (required) | Routing of a CDP object (signal, property etc.)
[any other attribute] | Passed directly to inner `input` element. Can be used to customize the input. Some useful ones are attributes `class` and `type`.

Following examples show different types of inputs. Samples use [Semantic UI](https://semantic-ui.com/) for styling.

#### Text input

    <cdp-input class="ui input" routing="webApp.Description" />

![Text input image](https://gitlab.com/karmo/angular-cdp/raw/master/images/input-text.png)

#### Checkbox

    <div class="ui checkbox">
        <cdp-input type="checkbox" routing="webApp.OverrideStateMachine"></cdp-input>
        <label>Checkbox</label>
    </div>

![Checkbox image](https://gitlab.com/karmo/angular-cdp/raw/master/images/input-checkbox.png)

#### Toggle button

    <div class="ui toggle checkbox">
        <cdp-input type="checkbox" routing="webApp.OverrideStateMachine"></cdp-input>
        <label>Override State Machine</label>
    </div>

![Toggle button image](https://gitlab.com/karmo/angular-cdp/raw/master/images/input-toggle.png)

#### Number input

    <cdp-input class="ui input" type="number" routing="webApp.Debug" />

![Number input image](https://gitlab.com/karmo/angular-cdp/raw/master/images/input-number.png)

#### Range input

    <cdp-input type="range" min="0" max="100" step="0.01" value="40" routing="webApp.CDPSignal"></cdp-input>
    
![Range input image](https://gitlab.com/karmo/angular-cdp/raw/master/images/input-range.png)
